My first react app.
Following a tutorial on the [cloudboost blog](https://blog.cloudboost.io/creating-a-chat-web-app-using-express-js-react-js-socket-io-1b01100a8ea5).

It is a simple real-time chat app based on socketio.

To run the app:

1. Clone the repo. `git clone https://mekicha@bitbucket.org/mekicha/first-react-app.git`

2. cd to the project. Something like `cd first-react-app`

3. Install the dependencies. Running `npm install` should do the magic.

4. Run the server by typing `nodemon backend/app.js`. I assume you have `nodemon` installed globally on your machine. If that's not the case, you can add it as a dependency by typing `npm i --save nodemon`.

5. Run the *react* app. Type `npm start`. This should open the browser to `localhost:3000` for you.

6. That's it. Enter a username and message and click send. Oh, and open a few tabs to see that the message is being broadcast to all connected clients.

7. Thank you!