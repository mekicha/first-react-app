import React from 'react';
import io from 'socket.io-client';

class Chat extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            username: '',
            message: '',
            messages: []
        };

        this.socket = io('localhost:8080');

        this.sendMessage = ev => {
            ev.preventDefault();
            this.socket.emit('SEND_MESSAGE', {
                author: this.state.username,
                message: this.state.message
            });
            this.setState({message: ''});
        }

        this.socket.on('RECEIVE_MESSAGE', (data) => {
            addMessage(data);
        });

        const addMessage = data => {
            console.log('data', data);
            this.setState({messages: [...this.state.messages, data]});
            console.log(this.state.messages);
        };

        this.handleUsernameChange = e => {
            this.setState({username: e.target.value});
        }

        this.handleMessageChange = e => {
            this.setState({message: e.target.value});
        }

    }
    render(){
        return (
            <div className="container">
                <div className="row">
                    <div className="col-4">
                        <div className="card">
                            <div className="card-body">
                                <div className="card-title">Global Chat</div>
                                <hr/>
                                <div className="messages">
                                    {this.state.messages.map(message =>{
                                        return (
                                            <div>{message.author}: {message.message}</div>
                                        )
                                    })}
                                </div>
                            </div>
                        <div className="card-footer">
                            <input type="text" placeholder="Username" className="form-control" onChange={this.handleUsernameChange}/>
                            <br/>
                            <input type="text" placeholder="Message" className="form-control" onChange={this.handleMessageChange}/>
                            <br/>
                            <button onClick={this.sendMessage} className="btn btn-primary form-control">Send</button>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        
        );
    }
}

export default Chat;